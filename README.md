# Number Formatter

Format numbers using Swiss German language style

[![npm version](https://badge.fury.io/js/__packageName__.svg)](https://badge.fury.io/js/__packageName__)



## Installation

```bash
yarn add @ta-interaktiv/number-formatter
```

## API

## Contributing

Code resides in `/src`. 

This repository follows the Standard JS style. To fix your code, run

```bash
yarn run fix
```

[![JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

Type checking is provided by [Flow](https://flow.org).

```bash
yarn run flow
```

### Styling

Styles reside in `/style` and/or `/scss`. 

### Testing

Test cases are provided using
[Storybook](https://storybook.js.org). Add test cases in `/stories/index.js`,
and run them with

```bash
yarn run storybook
```

They will appear on <http://localhost:9001>.

Automated tests are written for Jest and can be found in `/test`. Run the 
test suite using

```bash
yarn run test
```

### Transpiling

Babel is used to provide ES5 compatible code. To compile run

```bash
yarn run dist
```

### Documentation

Documentation is generated from source, using both JSDoc comments as well as 
Flow type annotations. Update the documentation in this file using

```bash
yarn run docs
```

### Changelog

The changelog is generated from git commits. In order to update the 
changelog, you may use 

```bash
yarn run changelog
```

– however, ever better is

### Versioning

For correct tagging, updating the changelog etc. you can use the appropriate 
NPM scripts.

These scripts will

1.  Bump the package.json version number
2.  Update the documentation
3.  Update the changelog
4.  Commit these changes
5.  Tag the commit with the correct version number

For project changes that improve the code in some way, but **don't** change the 
API in any way, use 

```bash
npm version patch
```

For project changes that **add** to the API or add functionality but will not 
affect the functioning of older implementations, use

```bash
npm version minor
```

For project changes that change the component in a fundamental way that would
 require code that implemented an older version of this component to be 
 rewritten, use

```bash
npm version major
```

### Publishing

In order to publish the new version of the package, push the new version to 
the repository and publish on NPM:

```bash
git push --follow-tags
npm publish
```
